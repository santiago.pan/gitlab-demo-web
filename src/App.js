import React, { useEffect, useState } from "react";
import logo from "./gitlab-logo.svg";
import "./App.css";

const API = "https://gitlab-demo-server.herokuapp.com";
const GET_STAGES = `${API}/stages`;
const POST_STAGE = `${API}/stages`;
const RESET_STAGES = `${API}/reset`;

function App() {
  const [stages, setStages] = useState([]);
  const [stage, setStage] = useState([]);

  useEffect(() => {
    fetch(GET_STAGES)
      .then(response => response.json())
      .then(response => setStages(response.stages));
  }, []);

  function handleOnSubmit(event) {
    fetch(POST_STAGE, {
      method: "POST",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ stage })
    })
      .then(response => response.json())
      .then(response => setStages(response.stages));
    setStage("");

    event.preventDefault();
  }

  function handleOnClick(event) {
    fetch(RESET_STAGES)
      .then(response => response.json())
      .then(response => setStages(response.stages));

    event.preventDefault();
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">GitLab is a cool company</h1>
      </header>
      <div className="App-body">
        <h2 className="App-stages-title">Stages of the DevOps lifecycle:</h2>
        {stages.length === 0 && <div className="App-loading">Loading...</div>}
        <div className="App-stages">
          {stages.map((stage, index) => (
            <h4
              className="App-stage"
              key={index}
              // NOTE: Intentionally added for XSS demo purpose
              dangerouslySetInnerHTML={{ __html: stage }}
            />
          ))}
        </div>
        <form className="App-form" onSubmit={handleOnSubmit}>
          <label>
            <input
              className="App-submit-input"
              placeholder="Add new stage..."
              value={stage}
              onChange={event => setStage(event.target.value)}
              type="text"
              name="name"
            />
          </label>
          <input className="App-submit-button" type="submit" value="Submit" />
          <button className="App-submit-button" onClick={handleOnClick}>
            Reset
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
